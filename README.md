# Grub Bootloader for NetifyOS

## Description
The GRand Unified Bootloader (GRUB) is a highly configurable and customizable bootloader with modular architecture.  It supports a rich variety of kernel formats, file systems, computer architectures and hardware devices.

## Changes for NetifyOS
A small change is required to change the path of a UEFI tool.  Though the release file can be used to detect the path, it requires building the package on a NetifyOS system.

In addition, some permissions were made more restrictive.

## Maintenance
To pull in an upstream update, follow these instructions:

  * git clone git@gitlab.com:netify.ai/netifyos/grub2.git
  * cd grub2
  * git checkout c7
  * git remote add upstream git://git.centos.org/rpms/grub2.git
  * git pull upstream c7
  * git checkout netifyos7
  * git merge --no-commit c7
  * git commit
